#include "threads.h"


#define EASY 1000
#define MEDIUM 100000
#define HARD 1000000
int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(0, EASY, primes1);
	printVector(primes1);
	getPrimes(0, MEDIUM, primes1);//too long to print
	getPrimes(0, HARD, primes1);//too long to print
	vector<int> primes2 = callGetPrimes(0, EASY);
	printVector(primes2);
	primes2 = callGetPrimes(0, MEDIUM);//too long to print
	callGetPrimes(0, HARD);//too long to print

	callWritePrimesMultipleThreads(1, EASY, "primes1.txt", 5);
	callWritePrimesMultipleThreads(1, MEDIUM, "primes2.txt", 5);
	callWritePrimesMultipleThreads(1, HARD, "primes3.txt", 5);

	system("pause");
	return 0;
}