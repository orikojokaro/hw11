#include "threads.h"
#define DELETED -1
//this function leaves the -1s in the vector. (so any number from begin to end that isn't a prime will be -1)
void removeNonPrimes(int begin, int end, std::vector<int>& primes)
{
	int i = 0;
	for (int i = 2; i < end; i++) primes.push_back(i);

	if (begin <= 1)
		begin = 2;
	while (true)
	{
		if (primes[i] * primes[i] > end)
		{
			break;
		}
		if (primes[i] == DELETED)
		{
			i++;
			continue;
		}
		for (int k = primes[i] * 2; k < end; k += primes[i])
		{
			primes[k - 2] = DELETED;
		}
		i++;
		while (primes[i] == DELETED)
		{
			i++;
		}
	}
}
void I_Love_Threads()
{
	std::cout << "I love threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread t(I_Love_Threads);
	t.detach();
}

void printVector(vector<int> primes)
{
	int len = primes.size();
	for (int i = 0; i < len; i++)
	{
		std::cout << primes[i] << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	std::vector<int> vec;
	int i = 0;
	int len = 0;
	removeNonPrimes(begin, end, vec);
	len = vec.size();
	i = 0;
	while (i < len)
	{
		if (vec[i] != DELETED)
			primes.push_back(vec[i]);
		i++;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	std::thread t(getPrimes, begin, end, std::ref(primes));
	t.join();
	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	std::vector<int> primes;
	int len = 0;
	int i = 0;
	CRITICAL_SECTION cs;
	InitializeCriticalSection(&cs);
	removeNonPrimes(begin, end, std::ref(primes));
	len = primes.size();
	while (i < len)
	{
		if (primes[i] != DELETED)
		{
			EnterCriticalSection(&cs);
			file << primes[i] << std::endl;
			LeaveCriticalSection(&cs);
		}
		i++;
	}
	DeleteCriticalSection(&cs);
}
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{

	int step = end / N;
	ofstream file;
	std::vector<std::thread *> allThreads;
	clock_t time = clock();
	std::thread *t = 0;
	file.open(filePath, ios::out);
	for (int i = 0; i < N; i++)
	{
		t = new std::thread(writePrimesToFile, i * step, i * step + step, std::ref(file));
		allThreads.push_back(t);
	}
	for (int i = 0; i < (int)allThreads.size(); i++)
	{
		allThreads[i]->join();
		delete allThreads[i];
	}
	std::cout << "time passed: " << clock() - time << " milliseconds" << std::endl;
}
